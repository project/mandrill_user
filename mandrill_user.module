<?php
/**
 * @file
 * Main module functions for mandrill_user.
 */

/**
 * Implements hook_permission().
 */
function mandrill_user_permission() {
  return array(
    'reject mandrill address' => array(
      'title' => t('Reject Mandrill address'),
      'description' => t('Add an email address the Mandrill unsubscribe list.'),
    ),
  );
}

/**
 * Mandrill user settings.
 *
 * This is the beginnings of a separate page to handle Mandrill subscription.
 */
function mandrill_user_account($form, &$form_state) {
  $form = array();

  $email = $form_state['build_info']['args'][0]->mail;

  $form['check'] = array(
    '#type' => 'checkbox',
    '#title' => t('Unsubscribe !e from mailings', array('!e' => $email)),
    '#default_value' => TRUE,
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));

  return $form;
}

/**
 * Implements hook_form_alter().
 */
function mandrill_user_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'user_profile_form':
      $form['account']['status']['mandrill_reject'] = array(
        '#type' => 'checkbox',
        '#title' => t('Unsubscribe %e from Mandrill mailings if blocking user.',
          array('%e' => $form['#user']->mail)),
        '#description' => t('This option is ignored if user is Active.'),
        '#default_value' => TRUE,
        '#weight' => 10,
      );
      break;

    case 'user_cancel_confirm_form':
      $form['mandrill'] = array(
        '#type' => 'fieldset',
        '#title' => t('Mandrill Mailing List Option'),
      );
      $form['mandrill']['mandrill_reject'] = array(
        '#type' => 'checkbox',
        '#title' => t('Unsubscribe %e from Mandrill mailings.',
          array('%e' => $form['_account']['#value']->mail)),
        '#default_value' => TRUE,
        '#weight' => 10,
      );
      break;

    default:
      // Do nothing with other forms.
      return;
  }

  $form['#submit'][] = 'mandrill_user_submit';
}

/**
 * Submit handler.
 */
function mandrill_user_submit($form, &$form_state) {
  switch ($form['#form_id']) {
    case 'user_profile_form':
      if ($form_state['values']['mandrill_reject'] && !$form_state['values']['status']) {
        mandrill_user_reject($form['#user']->mail);
      }
      break;

    case 'user_cancel_confirm_form':
      if ($form_state['values']['mandrill_reject']) {
        mandrill_user_reject($form['_account']['#value']->mail);
      }
      break;

    default:
      return;
  }
}

/**
 * Unsubscribe an email address by calling the reject API.
 *
 * @link https://mandrillapp.com/api/docs/rejects.JSON.html#method-add
 *
 * @return bool
 *   True on success.
 */
function mandrill_user_reject($email) {
  if (!user_access('reject mandrill address')) {
    drupal_set_message('Insufficient permissions to unsubscribe user from Mandrill.', 'warning');
    return FALSE;
  }

  $mailer = mandrill_user_get_api_object();
  $result = $mailer->reject($email);

  if (!empty($result['added']) && $result['added'] == TRUE) {
    drupal_set_message(t('Email address %e added to Mandrill unsubscribed list.',
      array('%e' => $email)));
    return TRUE;
  }
  drupal_set_message(t('Problem while adding email address %e to Mandrill unsubscribed list.',
    array('%e' => $email)), 'error');
  return FALSE;
}

/**
 * Return Mandrill API object for communication with the mailchimp server.
 * @see mandrill_get_api_object()
 */
function mandrill_user_get_api_object($reset = FALSE) {
  $api =& drupal_static(__FUNCTION__, NULL);

  if ($api === NULL || $reset === TRUE) {
    $api_key = variable_get('mandrill_api_key', '');
    $api_timeout = variable_get('mandrill_api_timeout', 60);
    if (empty($api_key)) {
      return FALSE;
    }

    $api = new MandrillUser($api_key, $api_timeout);
  }

  return $api;
}
