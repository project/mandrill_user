<?php

/**
 * @file
 * MandrillUser class to extend the Mandrill class.
 */

/**
 * Class MandrillUser.
 */
class MandrillUser extends Mandrill {

  /**
   * Add an email address to the reject list.
   *
   * @link https://mandrillapp.com/api/docs/rejects.JSON.html#method-add
   *
   * @return array|MandrillException
   *   Results of request or the exception.
   */
  public function reject($email) {
    return $this->request('rejects/add',
      array(
        'email' => $email,
        'comment' => t('Drupal accout cancelled.'),
      )
    );
  }
}
